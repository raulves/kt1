
public class Sheep {

   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // for debugging
   }
   
   public static void reorder (Animal[] animals) {
      if(animals == null) {
         throw new RuntimeException();
      }

      int goatCount = 0;
      for (Animal animal : animals) {
         if (animal == Animal.goat) {
            goatCount++;
         }
      }

      for(int i = 0; i < animals.length; i++) {
         if (goatCount > 0) {
            animals[i] = Animal.goat;
            goatCount--;
         } else {
            animals[i] = Animal.sheep;
         }
      }
   }

}

